<#
    .SYNOPSIS
        Unzip file from DEMO Servers.
    .DESCRIPTION
        This script allow use 
#>

$Origin_Folder = "..\origin\"
$Destionation_Folder = "..\destination\"
$Today = (Get-Date).ToString('yyyy-MM-dd')
$Time = (Get-Date).ToString('HH:MM')
$FileTime = (Get-Date).ToString('HHMM')
$Files = Get-ChildItem $Origin_Folder*.zip -Name

# Functions
Function Log {
  param(
      [Parameter(Mandatory=$true)][String]$msg
  )
  
  Add-Content execution-log-$Today-$FileTime.log $msg
}

# Main Task

## Create execution log

Log "------------------------------------"
Log "Starting Execution DEMO Unzip Process"
Log "Date: $Today Time: $Time"
Log "------------------------------------"

## Unzip 

foreach($file in $Files)
{
  $CheckSum = Get-FileHash -Path $Origin_Folder$file
  Expand-Archive -Path $Origin_Folder$file -DestinationPath $Destionation_Folder
  Log "$file - $CheckSum"
}